import 'package:flutter/material.dart';
import 'package:flutter_file_dialog/flutter_file_dialog.dart';
import 'package:image_meta_data/image_meta_navigator.dart';

import 'image_meta_components.dart';

void main() {
  runApp(const ImageMetaApp());
}

class ImageMetaApp extends StatelessWidget {
  const ImageMetaApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ImageMeta',
      theme: ThemeData(brightness: Brightness.dark),
      themeMode: ThemeMode.dark,
      home: const ImageMetaHomePage(),
    );
  }
}

class ImageMetaHomePage extends StatefulWidget {
  const ImageMetaHomePage({Key? key}) : super(key: key);

  @override
  State<ImageMetaHomePage> createState() => _ImageMetaHomePageState();
}

class _ImageMetaHomePageState extends State<ImageMetaHomePage> {
  ImageMetaNavigator navigator = ImageMetaNavigator();

  void _openFileUploadDialogue() {
    Future<String?> file = FlutterFileDialog.pickFile(
      params: const OpenFileDialogParams(
        dialogType: OpenFileDialogType.image,
        sourceType: SourceType.photoLibrary,
        allowEditing: false,
        localOnly: false,
        copyFileToCacheDir: true,
      )
    );
    
    file.then((imagePath) => {
      navigator.openMetaDataScreen(context, imagePath!)
    });
  }

  void _openGalleryScreen() {
    navigator.openGalleryScreen(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ImageMetaComponents.imageMetaAppBar('ImageMeta'),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            IconButton(
              onPressed: _openFileUploadDialogue,
              icon: const Icon(Icons.upload_rounded),
              iconSize: 300,
              color: Colors.grey,
              tooltip: 'Bild hochladen',
            ),
            ImageMetaComponents.imageMetaTextWidget('Bild hochladen'),
            IconButton(
              onPressed: _openGalleryScreen,
              icon: const Icon(Icons.storage_rounded),
              iconSize: 300,
              color: Colors.grey,
              tooltip: 'Galerie',
            ),
            ImageMetaComponents.imageMetaTextWidget('Galerie')
          ],
        ),
      ),
    );
  }
}
