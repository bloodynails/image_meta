import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:exif/exif.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;

import 'image_meta_components.dart';

class MetaDataPage extends StatefulWidget {
  const MetaDataPage({Key? key, required this.imagePath}) : super(key: key);
  final String imagePath;

  @override
  State<MetaDataPage> createState() => _MetaDataPageState();
}

class _MetaDataPageState extends State<MetaDataPage> {
  _readExifData() async {
    var imageFile = File(widget.imagePath);
    var fileName = path.basename(imageFile.path);
    var bytes = imageFile.readAsBytesSync();
    var exifData = await readExifFromBytes(bytes);
    var applicationDocumentsDir = await getApplicationDocumentsDirectory();
    var imageDir = await Directory(applicationDocumentsDir.path + '/images').create(recursive: true);
    var metadata = '';

    await imageFile.copy('${imageDir.path}/$fileName');

    exifData.forEach((key, value) {
      metadata += '$key: $value;;';
    });

    return metadata;
  }
  _getTextWidgets(metadata) {
    var textWidgets = List<Text>.empty(growable: true);
    metadata = metadata.split(';;');

    metadata.forEach((value) => {
      textWidgets.add(Text(value))
    });

    return textWidgets;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ImageMetaComponents.imageMetaAppBar('Metadaten'),
      body: Center(
        child: FutureBuilder(
          future: _readExifData(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasError && kDebugMode) {
              print(snapshot.error.toString());
            }

            if (!snapshot.hasData) {
              return const CircularProgressIndicator();
            }

            return SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Image.file(File(widget.imagePath), height: 200),
                  const SizedBox(height: 20),
                  ..._getTextWidgets(snapshot.data),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}