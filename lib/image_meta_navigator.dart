import 'package:flutter/material.dart';
import 'package:image_meta_data/main.dart';

import 'gallery_screen.dart';
import 'metadata_screen.dart';

class ImageMetaNavigator {
  void openGalleryScreen(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(
        builder: (BuildContext context) => const GalleryPage(title: 'Galerie')
    ));
  }

  void openMetaDataScreen(BuildContext context, String imagePath) {
    Navigator.push(context, MaterialPageRoute(
        builder: (BuildContext context) => MetaDataPage(imagePath: imagePath)
    ));
  }

  void openHomeScreen(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(
        builder: (BuildContext context) => ImageMetaHomePage()
    ));
  }

  void clearNavigator(BuildContext context) {
    Navigator.popUntil(context, (route) => false);
  }
}