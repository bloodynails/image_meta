import 'package:flutter/material.dart';

class ImageMetaComponents {
  static AppBar imageMetaAppBar(title) {
    return AppBar(
      title: Center(child: Text(title)),
      leading: const IconButton(onPressed: null, icon: Icon(Icons.person)),
      actions: const <Widget>[
        IconButton(onPressed: null, icon: Icon(Icons.settings))
      ],
    );
  }
  
  static Text imageMetaTextWidget(text) {
    return Text(
      text,
      style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18)
    );
  }
}