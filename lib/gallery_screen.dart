import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart';
import 'package:image_meta_data/image_meta_navigator.dart';

import 'image_meta_components.dart';

class GalleryPage extends StatefulWidget {
  const GalleryPage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<GalleryPage> createState() => _GalleryPageState();
}

class _GalleryPageState extends State<GalleryPage> {
  var imageNavigator = ImageMetaNavigator();
  var images = List<Image>.empty(growable: true);

  _openMetaDataScreen(String imagePath) {
    // TODO: Fix this
    imageNavigator.clearNavigator(context);
    imageNavigator.openMetaDataScreen(context, imagePath);
  }

  _getGalleryImages() async {
    var rowsAndBoxes = List<Widget>.empty(growable: true);
    var applicationDocumentsDir = await getApplicationDocumentsDirectory();
    var imageDir = await Directory(applicationDocumentsDir.path + '/images').create(recursive: true);
    var fileList = imageDir.listSync();

    for (var file in fileList) {
      rowsAndBoxes.add(
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              onPressed: () => _openMetaDataScreen(file.path),
              icon: Image.file(File(file.path)),
              iconSize: 150,
              padding: EdgeInsets.zero,
            ),
            const IconButton(onPressed: null, icon: Icon(Icons.delete))
          ],
        )
      );
    }

    return rowsAndBoxes;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ImageMetaComponents.imageMetaAppBar('Galerie'),
      body: Center(
        child: FutureBuilder(
          future: _getGalleryImages(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasError && kDebugMode) {
              print(snapshot.error.toString());
            }

            return snapshot.hasData
              ? ListView(children: snapshot.data)
              : const CircularProgressIndicator();
          }
        ),
      )
    );
  }
}